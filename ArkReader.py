

class StateObj:
    """ Common class for State Pattern """

    def __init__(self, audio_file_name="", feature_vectors=None):
        """ cur_audio_file_name and features_vector nested for all states

            features_vector - dict() in format  {audio_file_name: [[ftr1], .., [ftrN]], ...}
                will be filling in read_line process StateReadingFtrs and transfer to the next states by __init__
        """

        self.cur_audio_file_name = audio_file_name
        self.features_vector = feature_vectors

    def read_line(self, line_to_parse):
        raise RuntimeError('Error! Ark reader object not in a available state')


class StateWaitingName(StateObj):
    """ Ark file should start with 'audio_file_name [' """

    def read_line(self, line_to_parse):
        """ """

        ''' If line is empty do not change the state '''
        if len(line_to_parse.strip()) == 0:
            return self

        ''' Split line into 2 part [audio_file_name, ']'] '''
        line_to_parse = line_to_parse.strip().split()

        ''' Ark,t file should starts with audio_file_name and ']' '''
        if len(line_to_parse) != 2 or line_to_parse[1] != '[':
            raise RuntimeError('File with features should have ark,t format' +
                               ' and starts features with audio_file_name [')

        ''' Move to next state reading '''
        return StateReadingFtrs(line_to_parse[0], self.features_vector)


class StateReadingFtrs(StateObj):
    """ Reading features in format: ftr1 ftr2 .. ftrN or ftr1 ftr2 .. ftrN ] """

    def __init__(self, audio_file_name, features_vector=None):
        """ Set mandatory for audio file name """

        ''' Check if feature with the same name already exist '''
        if (features_vector and
            audio_file_name in features_vector):
            raise RuntimeError('Feature with name "{}" already read'.format(audio_file_name))

        ''' Initialize features_vector if it is None '''
        if not features_vector:
            features_vector = {}

        ''' Add new audio file to features vectors '''
        features_vector[audio_file_name] = []

        super().__init__(audio_file_name, features_vector)

    def read_line(self, line_to_parse):
        """ """

        ''' Split line into features and may to appear a ']' at the end '''
        line_to_parse = line_to_parse.strip().split()

        ''' Init vector for a feature '''
        self.features_vector[self.cur_audio_file_name].append([])

        ''' Read elem for feature vector '''
        for _ftr_elem in line_to_parse:

            # print(_ftr_elem)

            if _ftr_elem.lstrip('-').replace('.', '', 1)\
                                    .replace('e', '')\
                                    .replace('-', '')\
                                    .replace('+', '').isdigit():
                ''' Get a float feature's elem '''
                self.features_vector[self.cur_audio_file_name][-1].append(float(_ftr_elem))

            elif (_ftr_elem.find(']') != -1 and
                  len(self.features_vector[self.cur_audio_file_name][-1]) > 0):
                ''' Reach the end of features vector.
                Move to the End state of reading features vectors for a audio file'''
                return StateEdnFtrs(self.cur_audio_file_name,
                                    self.features_vector)

            else:
                raise RuntimeError('Read from Ark file {} is not a float or floats + "]"'.format(line_to_parse))

        return self


class StateEdnFtrs(StateObj):
    """ Found line with ']' """

    def __init__(self, audio_file_name, feature_vectors):
        """ Set mandatory for audio file name and feature_vectors """
        super().__init__(audio_file_name, feature_vectors)

    def read_line(self, line_to_parse):
        """  """

        new_state = StateWaitingName(self.cur_audio_file_name, self.features_vector)

        return new_state.read_line(line_to_parse)


# /////////////////////////////////////////////////////////
# //                    ArkReader
class ArkReader:
    """ Read files in ark,t format """

    def __init__(self):
        """ Constructor """

        ''' Set initial state '''
        self.state = StateWaitingName("")

    def read_line(self, line_to_parse):
        self.state = self.state.read_line(line_to_parse)

    def read_file(self, file_name):
        for _line in open(file_name).readlines():
            self.read_line(_line)

    def get_feature(self, audio_file_name):
        return self.state.features_vector[audio_file_name]

    def get_all_file_names(self):
        return list(self.state.features_vector.keys())

    def get_all_features(self):
        return self.state.features_vector


# TEST
#
# import numpy as np
#
# obj = ArkReader()
#
# obj.read_file('data/files.txtftr')
#
# # for _line in open('data/files.txtftr').readlines():
# #     obj.read_line(_line)
#
# print(sorted(obj.get_all_features().items()))
#
# print(np.array(obj.get_feature('file1')))
# print(obj.get_all_file_names())
