import os
import numpy as np
import pickle


class AcoModel:
    """  """

    def dist(self, ftr_input):
        # return np.sqrt((ftr_input - self.trained_phoneme)**2)
        # vec1 = np.multiply(ftr_input,            [1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        # vec2 = np.multiply(self.trained_phoneme, [1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

        vec1 = np.multiply(ftr_input           , [1.0, 0.9, 0.4, 0.4, 0.4, 0.0, 0.0, 0.0, 0.1, 0.1, 0.05, 0.05, 0.01])
        vec2 = np.multiply(self.trained_phoneme, [1.0, 0.9, 0.4, 0.4, 0.4, 0.0, 0.0, 0.0, 0.1, 0.1, 0.05, 0.05, 0.01])

        # return np.linalg.norm(ftr_input - self.trained_phoneme)
        return np.linalg.norm(vec2 - vec1)

    def __init__(self, sort_dir, fn_sort_file):

        self.name = fn_sort_file

        self.trained_phoneme \
            = AcoModel.train_phoneme(AcoModel.read_phoneme(sort_dir + '/' + fn_sort_file))

    @staticmethod
    def calc_euclid_dist(ftr1, ftr2):
        return np.linalg.norm(ftr1 - ftr2)

    @staticmethod
    def train_phoneme(phoneme_vectors):
        """ Train a phoneme """

        ''' raise an exception if has not any feature vector in a file '''
        if len(phoneme_vectors) == 0:
            raise RuntimeError('Phoneme vectors has zero len')

        ''' Initialize container for the mean '''
        average_phoneme = np.zeros(np.shape(phoneme_vectors[0]), dtype=np.float)

        '''' Calc mean '''
        for _ph in phoneme_vectors:
            average_phoneme += _ph

        average_phoneme /= len(phoneme_vectors)
        # print('old average : ', average_phoneme)
        #
        # for i in range(5):
        #     ''' Remove farthest 10% of features '''
        #     phoneme_vectors = sorted(phoneme_vectors, key=lambda x: AcoModel.calc_euclid_dist(average_phoneme, x))
        #     phoneme_vectors = phoneme_vectors[:-int((len(phoneme_vectors)/5))]
        #
        #     ''' Initialize container for the mean '''
        #     average_phoneme = np.zeros(np.shape(phoneme_vectors[0]), dtype=np.float)
        #
        #     '''' Calc mean '''
        #     for _ph in phoneme_vectors:
        #         average_phoneme += _ph
        #
        #     average_phoneme /= len(phoneme_vectors)
        #
        #     print('new average : ', average_phoneme)

        return average_phoneme

    @staticmethod
    def read_phoneme(file_name):

        phoneme = []

        for _ftr_vector in open(file_name).readlines():
            phoneme.append(_ftr_vector.strip().split())

        return np.array(phoneme, dtype=np.float)


class AcoModelSet:
    """  """

    def find_model(self, model_name):
        return self.name2model[model_name]

    def save(self, fname):
        with open(fname, "wb") as f:
            pickle.dump(self, f)

    def __init__(self, sort_dir, save_to_file):

        self.name2model = {}

        print("Training...")
        for fn_model in os.listdir(sort_dir):
            self.name2model[fn_model] = AcoModel(sort_dir, fn_model)

        print("...saving")
        self.save(save_to_file)
        print("Done")

    def print_all(self):
        for _, _model in self.name2model.items():
            print('model "{}" : {}'.format(_model.name, _model.trained_phoneme))

    @staticmethod
    def load_aco_model_set(fname):

        with open(fname, "rb") as f:
            q = pickle.load(f)

        return q


if __name__ == '__main__':
    AcoModelSet('sort_dir', 'data/ph_models')

    model = AcoModelSet.load_aco_model_set('data/ph_models')
    print(model.find_model('a').name)
    print(model.find_model('a').trained_phoneme)

    model.print_all()

# TEST
#
# AcoModelSet('sort_dir', 'data/ph_models')
#
# model = AcoModelSet.load_aco_model_set('data/ph_models')
# print(model.find_model('a').name)
# print(model.find_model('a').trained_phoneme)
#
# model.print_all()

# TEST VoxForge
#
# AcoModelSet('VoxForge/sort_dir', 'data/ph_models')
#
# model = AcoModelSet.load_aco_model_set('data/ph_models')
# print(model.find_model('a').name)
# print(model.find_model('a').trained_phoneme)
#
# model.print_all()