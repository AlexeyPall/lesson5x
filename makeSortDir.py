import sys
from collections import defaultdict

from ArkReader import ArkReader


class MakeSortDir:
    """ """

    def __init__(self, *, tra_file, ark_file):

        self.translations = MakeSortDir.read_file_translation(tra_file)
        self.features = MakeSortDir.read_ftr_file(ark_file)

    @staticmethod
    def read_file_translation(file_name):
        """ Read file in format:
            audio_file_nameX ph1 ph2 .. pnN
            ...

            Returns dict in format:
                {audio_file_nameX: [ph, ...], ...}
         """

        ''' Initialize result dict '''
        translation = {}

        ''' Read translations from the file '''
        for _line in open(file_name).readlines():
            if len(_line.strip()) > 0:
                translation[_line.strip().split()[0]] = _line.strip().split()[1:]

        return translation

    @staticmethod
    def read_ftr_file(ftr_file):
        """ Read features in ark,t format

            Returns dict in format:
                {audio_file_nameX: [[ftr1], [ftr2], ...], ...}
        """

        ''' Init ark file reader '''
        ark_file_reader = ArkReader()

        ''' Read features from the file '''
        idx = 0
        for _line in open(ftr_file).readlines():
            ark_file_reader.read_line(_line)

            idx += 1
            if idx % 10000 == 0:
                print('has read: {} lines in states {}'.format(idx, len(ark_file_reader.state.features_vector)))

        return ark_file_reader.get_all_features()

    def fill_sort_dir(self, *, sort_folder):

        phonemes = defaultdict(list)

        for _file, _transl in self.translations.items():

            ''' Len of frames in translation should be equal to len of frames in features '''
            if len(_transl) != len(self.features[_file]):
                raise RuntimeError('Len of frames in translation should be equal to len of frames in features'
                                   ' for file {}'.format(_file))

            ''' Fill phonemes vectors '''
            for _frame_idx in range(len(_transl)):
                phonemes[_transl[_frame_idx]].append(self.features[_file][_frame_idx])

        ''' Save phonemes in files '''
        for _ph, _ftrs in phonemes.items():
            f = open(sort_folder + '/' + str(_ph), 'w')
            for _feature in _ftrs:
                f.write(str(_feature).strip('[').strip(']').replace(',', ' ') + '\n')


if __name__ == '__main__':

    if len(sys.argv) != 4:
        print('not all input params set:\n'
              '\tmakeSortDir.py <ali-file-name> <ftr-ark-file> <out-sort-dir>')
        sys.exit(1)

    obj = MakeSortDir(tra_file=sys.argv[1],
                      ark_file=sys.argv[2])

    obj.fill_sort_dir(sort_folder=sys.argv[3])


# TEST
#
# obj = MakeSortDir(tra_file='data/ali.tra',
#                   ark_file='data/files.txtftr')
#
# # print(obj.features)
# obj.fill_sort_dir(sort_folder='sort_dir')
