
from AcoModels import AcoModel
from AcoModels import AcoModelSet

from ArkReader import ArkReader
from Token import TokenPassing
from ConstructGraph import ConstructGraph


if __name__ == '__main__':

    DEBUG = '/home/alex/projects/pycharm/ASRlesson050/debug'

    PHONE_DIR = '/home/alex/projects/pycharm/ASRlesson050/VoxForge/ph_models'
    # ALI_FILE = '/home/alex/lessons/ASR/decoder/test_data/data/VoxforgeRu/ali/monoali.phoneNames'
    # ALI_FILE = '/home/alex/projects/pycharm/ASRlesson050/da_net/da_net.dic'
    ALI_FILE = '/home/alex/projects/pycharm/ASRlesson050/digits/digits.dic'

    # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson050/VoxForge/test_wav_features.txtftr'
    # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson050/da_net/da_net_etalons.txtftr'
    # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson050/da_net/da_net_test.txtftr'
    # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson050/da_net/street_da_net.txtftr'
    # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson050/digits/digits_etalons.txtftr'
    TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson050/digits/digits_test.txtftr'
    # TO_RECOGNIZE = '/home/alex/projects/pycharm/ASRlesson050/digits/street_digits.txtftr'

    print('Loading graph ...')
    aco_model = AcoModelSet.load_aco_model_set(PHONE_DIR)
    head_node = ConstructGraph.costruct(ALI_FILE, aco_model)

    # print('\n' + '#' * 30 + '\n' + ' Debug '.center(30, '~') + '\n' + '#'*30)
    # ConstructGraph.print_graph(head_node)

    features_to_recognize = ArkReader()
    features_to_recognize.read_file(TO_RECOGNIZE)

    engine = TokenPassing(head_node)

    patterns = 0
    errors = 0

    for _file in features_to_recognize.get_all_file_names():
        print('\nRecognize File : ', _file)
        # print('-' * 30 + '\n' + _file.center(30, '~') + '\n' + '-' * 30)
        word, final_tokens = engine.run(features_to_recognize.get_feature(_file))

        # if _file == '0_4.wav':
        #     TokenPassing.save_recognized_transcripts(final_tokens, DEBUG + '/0_4.debug.txt')

        patterns += 1

        if _file.split('_')[0].find(word) == -1:
            errors += 1
            print('ERROR')

    print('patterns : ', patterns)
    print('errors : ', errors)

